import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { UserFacadeService } from './user-facade.service';

describe('UserFacadeService', () => {
    let service: UserFacadeService;
    let httpMock: HttpTestingController;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [
                HttpClientTestingModule
            ]
        });

        service = TestBed.get(UserFacadeService);
        httpMock = TestBed.get(HttpTestingController);
    });

    it('should be created', () => {
        expect(service).toBeTruthy();
    });

    it('Should call getUsers method and expected to receive users', () => {
        const users = [
            {
                name: 'Nell D. Michael',
                phone: '602-1033',
                email: 'hendrerit.id.ante@placeratvelit.ca',
                company: 'Praesent Eu LLP',
                date_entry: '2017-07-30 23:27:39',
                org_num: '907369 2973',
                address_1: 'P.O. Box 916, 8584 Vestibulum St.',
                city: 'Vitry-sur-Seine',
                zip: '2353',
                geo: '60.77971, 7.98874',
                pan: '4532992507580',
                pin: '7086',
                id: 1,
                status: 'read',
                fee: '$60.99',
                guid: '48653E36-987F-48EC-7382-7F009FF34628',
                date_exit: '2018-11-14 12:37:54',
                date_first: '2018-05-20 01:07:05',
                date_recent: '2019-04-06 23:28:25',
                url: 'https://capco.com/'
            }
        ];
        service.getUsers().subscribe(response => {
            expect(response).toBe(users);
        });
        const req = httpMock.expectOne(`/assets/json/mock.json`);
        expect(req.request.method).toBe('GET');
        req.flush(users);
    });

    it('Should call sendUser method and expected to receive status', () => {
        const body = {
            id: 1,
            status: 'ok'
        };
        service.sendUser(body).subscribe(response => {
            expect(response.status).toBe('success');
        });
        const req = httpMock.expectOne(`/assets/json/mock.json`);
        expect(req.request.method).toBe('POST');
        req.flush({
            status: 'success'
        });
    });
});
