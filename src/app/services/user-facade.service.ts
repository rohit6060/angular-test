import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { User, UserPostRequest } from '../models/data.model';

@Injectable({
    providedIn: 'root'
})
export class UserFacadeService {

    constructor(private _http: HttpClient) { }

    getUsers(): Observable<User[]> {
        return this._http.get<User[]>('/assets/json/mock.json');
    }

    sendUser(data: UserPostRequest): Observable<any> {
        return this._http.post<any>('/assets/json/mock.json', { data });
    }
}
