import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UsersComponent } from './users.component';
import { TableComponent } from '../table/table.component';
import { FormsModule } from '@angular/forms';
import { TablePaginationComponent } from '../table/table-pagination/table-pagination.component';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { UserFacadeService } from 'src/app/services/user-facade.service';
import { of } from 'rxjs';
import { User } from 'src/app/models/data.model';

const user = {
    name: 'Nell D. Michael',
    phone: '602-1033',
    email: 'hendrerit.id.ante@placeratvelit.ca',
    company: 'Praesent Eu LLP',
    date_entry: '2017-07-30 23:27:39',
    org_num: '907369 2973',
    address_1: 'P.O. Box 916, 8584 Vestibulum St.',
    city: 'Vitry-sur-Seine',
    zip: '2353',
    geo: '60.77971, 7.98874',
    pan: '4532992507580',
    pin: '7086',
    id: 1,
    status: 'read',
    fee: '$60.99',
    guid: '48653E36-987F-48EC-7382-7F009FF34628',
    date_exit: '2018-11-14 12:37:54',
    date_first: '2018-05-20 01:07:05',
    date_recent: '2019-04-06 23:28:25',
    url: 'https://capco.com/'
};

describe('UsersComponent', () => {
    let component: UsersComponent;
    let fixture: ComponentFixture<UsersComponent>;
    const userSpy = jasmine.createSpyObj('UserFacadeService', ['getUsers', 'sendUser']);

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [FormsModule, HttpClientTestingModule],
            declarations: [UsersComponent, TableComponent, TablePaginationComponent],

        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(UsersComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it('Shoud call onUserSelect when user click on row', () => {
        const spy = spyOn(component['_userFacadeService'], 'sendUser').and.returnValue(of({}));
        component.onUserSelect(user);
        expect(spy).toHaveBeenCalled();
    });
});
