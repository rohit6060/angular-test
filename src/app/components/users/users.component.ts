import { Component, OnInit } from '@angular/core';
import { UserFacadeService } from 'src/app/services/user-facade.service';
import { map } from 'rxjs/operators';
import { User } from 'src/app/models/data.model';

@Component({
    selector: 'app-users',
    templateUrl: './users.component.html',
    styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit {

    displayColumns = ['id', 'name', 'email', 'phone', 'company', 'action'];
    users$ = this._userFacadeService.getUsers();

    constructor(private _userFacadeService: UserFacadeService) { }

    ngOnInit() {

    }

    onUserSelect(user: User): void {
        const { id, status } = user;
        this._userFacadeService.sendUser({ id, status }).subscribe(response => {
            console.log(response);
        });
    }

}
