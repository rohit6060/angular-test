import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TablePaginationComponent } from './table-pagination.component';

describe('TablePaginationComponent', () => {
    let component: TablePaginationComponent;
    let fixture: ComponentFixture<TablePaginationComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [TablePaginationComponent]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(TablePaginationComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });


    it('should assign the maxpage value when onChanges', () => {
        component.totalRecords = 200;
        component.perPage = 100;
        component.ngOnChanges();
        expect(component.maxPage).toEqual(1);
    });

    it('should emit the value when select box change', () => {
        const spy = spyOn(component['changePerPageCount'], 'emit');
        const event = {
            target: {
                value: 25
            }
        };
        component.changePerPages(event);
        expect(spy).toHaveBeenCalledWith(25);
    });

    it('Shoudl decrease page value and emit when click on Previous button', () => {
        component.page = 1;
        const spy = spyOn(component['changePage'], 'emit');
        component.previousPage();
        expect(spy).toHaveBeenCalledWith(0);
    });

    it('Shoudl not decrease page value and emit when page is in first', () => {
        component.page = 0;
        const spy = spyOn(component['changePage'], 'emit');
        component.previousPage();
        expect(spy).not.toHaveBeenCalledWith(0);
    });


    it('Should increase page value and emit when click on Next button', () => {
        component.page = 1;
        component.maxPage = 3;
        const spy = spyOn(component['changePage'], 'emit');
        component.nextPage();
        expect(spy).toHaveBeenCalledWith(2);
    });

    it('Should increase page value and emit when page is in last', () => {
        component.page = 3;
        component.maxPage = 3;
        const spy = spyOn(component['changePage'], 'emit');
        component.nextPage();
        expect(spy).not.toHaveBeenCalledWith(2);
    });

});
