import { Component, OnInit, Input, Output, EventEmitter, OnChanges } from '@angular/core';

@Component({
    selector: 'app-table-pagination',
    templateUrl: './table-pagination.component.html',
    styleUrls: ['./table-pagination.component.scss']
})
export class TablePaginationComponent implements OnInit, OnChanges {
    @Input() page: number;
    @Input() perPage: number;
    @Input() totalRecords: number;

    @Output() changePage = new EventEmitter<number>();
    @Output() changePerPageCount = new EventEmitter<number>();

    itemRanges = [5, 10, 25, 50, 100];

    maxPage: number;

    constructor() { }

    ngOnInit(): void {
    }

    ngOnChanges(): void {
        this.maxPage = Math.floor(this.totalRecords / this.perPage) - 1;
    }

    changePerPages(event: any): void {
        this.changePerPageCount.emit(event.target.value);
    }


    previousPage(): void {
        if (this.page > 0) {
            this.page = this.page - 1;
            this.changePage.emit(this.page);
        }
    }

    nextPage(): void {
        if (this.page < this.maxPage) {
            this.page = this.page + 1;
            this.changePage.emit(this.page);
        }
    }

}
