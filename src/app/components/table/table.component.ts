import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { User } from 'src/app/models/data.model';

@Component({
    selector: 'app-table',
    templateUrl: './table.component.html',
    styleUrls: ['./table.component.scss']
})
export class TableComponent implements OnInit {

    @Input() dataSource: Array<any>;
    @Input() displayColumns: Array<any>;

    @Output() actionSubmit = new EventEmitter<User>();

    page = 0;
    perPage = 25;


    constructor() { }

    ngOnInit(): void {
    }

    getColumnData(row: any, column: string): string {
        return (row[column] || '').toString();
    }

    selectAllRows(event: any): void {
        const { target: { checked: selected } } = event;
        this.dataSource = this.dataSource.map(data => ({ ...data, selected }));
    }

    onClick(row: any): void {
        this.actionSubmit.emit(row);
    }

    changePerPageCount(value: number): void {
        this.page = 0;
        this.perPage = value;
    }

    changePage(page: number): void {
        this.page = page;
    }
}
