import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TableComponent } from './table.component';
import { FormsModule } from '@angular/forms';
import { TablePaginationComponent } from './table-pagination/table-pagination.component';

describe('TableComponent', () => {
    let component: TableComponent;
    let fixture: ComponentFixture<TableComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [FormsModule],
            declarations: [TableComponent, TablePaginationComponent]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(TableComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it('should send value if property exist in object', () => {
        const row = {
            name: 'test'
        };
        const value = component.getColumnData(row, 'name');
        expect(value).toBe('test');
    });

    it('should send empty value if property does not exist object', () => {
        const row = {
            name: 'test'
        };
        const value = component.getColumnData(row, 'phone');
        expect(value).toBe('');
    });

    it('shoudl set selected true for all date if checkbox checked in header', () => {
        component.dataSource = [{
            name: 'test'
        }];
        const event = {
            target: {
                checked: true
            }
        };
        component.selectAllRows(event);
        const dataSource = component.dataSource.map(value => ({ ...value, selected: true }));
        expect(component['dataSource']).toEqual(dataSource);
    });

    it('should emit value when action button click', () => {
        const data = {
            name: 'test'
        };
        const spy = spyOn(component['actionSubmit'], 'emit');
        component.onClick(data);
        expect(spy).toHaveBeenCalledWith(data);
    });

    it('should change perPage values when select change', () => {
        const value = 20;
        component.changePerPageCount(value);
        expect(component['perPage']).toEqual(value);
    });

    it('should change page when user click on previous or next buttons', () => {
        const value = 1;
        component.changePage(value);
        expect(component['page']).toEqual(value);
    });
});
